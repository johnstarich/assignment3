package assignment3;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JComponent;

/**
 * Paints the board for Mastermind.
 * @author johnstarich
 */
public class Board extends JComponent
{
	/** Serialization ID needed for JComponents */
	private static final long serialVersionUID = 4034741150834196493L;
	
	private PegPainter[][] board;
	private Game game;
	
	/**
	 * Creates a Mastermind board with a back-reference to the instantiating game.
	 * @param game the provided game
	 */
	public Board(Game game)
	{
		super();
		this.game = game;
		this.board = new PegPainter[Game.INITIAL_REMAINING_GUESSES][Code.CODE_LENGTH*2];
	}
	
	/**
	 * Adds a guess to the board. The painter is then instructed to repaint.
	 * @param guess The guess submitted
	 * @param result The result for the guess
	 */
	public void addGuess(Code guess, Result result)
	{
		PegPainter[] row = board[game.getRemainingGuesses()];
		int col = 0;
		for(Peg peg : guess.getColors())
		{
			row[col] = new PegPainter(Peg.colorForChar(peg.getColor()));
			col += 1;
		}
		for(int i = result.getBlackPegs(); i > 0; i--)
		{
			row[col] = new PegPainter(Color.BLACK);
			col += 1;
		}
		for(int i = result.getWhitePegs(); i > 0; i--)
		{
			row[col] = new PegPainter(Color.WHITE);
			col += 1;
		}
		repaint();
	}
	
	@Override
	public Dimension getPreferredSize()
	{	return new Dimension(Code.CODE_LENGTH*2*(PegPainter.PEG_WIDTH+5)+50, Game.INITIAL_REMAINING_GUESSES*(PegPainter.PEG_HEIGHT+5));	}
	
	@Override
	public void paint(Graphics g)
	{
		Graphics2D g2 = (Graphics2D)g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.translate(10, PegPainter.PEG_HEIGHT/4);
		for(PegPainter[] row : board)
		{
			Graphics2D rowGraphics = (Graphics2D)g2.create();
			for(int i = 0; i < row.length; i++)
			{
				rowGraphics.setColor(Color.DARK_GRAY);
				rowGraphics.fillOval(PegPainter.PEG_WIDTH/4+1, PegPainter.PEG_HEIGHT*7/16, PegPainter.PEG_WIDTH/2, PegPainter.PEG_HEIGHT/8);
				PegPainter peg = row[i];
				if(peg != null)
				{
					peg.paint((Graphics2D)rowGraphics.create());
				}
				if(i == Code.CODE_LENGTH - 1)
					rowGraphics.translate(PegPainter.PEG_WIDTH+5, 0);
				rowGraphics.translate(PegPainter.PEG_WIDTH+5, 0);
			}
			g2.translate(0, PegPainter.PEG_HEIGHT+5);
		}
	}
}
