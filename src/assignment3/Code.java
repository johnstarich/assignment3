package assignment3;

import java.util.ArrayList;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Collection of colors used to represent a code
 * the player needs to guess and the guesses themselves.
 * @author johnstarich
 */
public class Code
{
	/** Standard code length for Mastermind. */
	public static final int CODE_LENGTH = 5;
	/** Pegs with the colors for this code. */
	private ArrayList<Peg> colors = new ArrayList<>();
	
	/**
	 * Creates a code parsed from the given input string.
	 * @param input string provided as the sequence of colored pegs represented as chars
	 */
	public Code(String input)
	{
		if(input == null)
		{	input = "";	}
		
		for(char c : input.toCharArray())
		{	colors.add(new Peg(c));	}
	}
	
	/**
	 * Creates a code with the given sequence of colored pegs.
	 * @param input given sequence of colored pegs
	 */
	public Code(ArrayList<Peg> input)
	{
		this.colors = input;
	}
	
	/**
	 * Returns the colors representing this peg
	 * @return this peg's colors
	 */
	public ArrayList<Peg> getColors()
	{
		return colors;
	}
	
	/**
	 * Validates whether or not this code is of the correct length and each peg is a valid code color.
	 * @throws Exception This throws if this code is invalid.
	 */
	public void validate() throws Exception
	{
		if(colors.size() != CODE_LENGTH)
		{	throw new Exception();	}
		
		for(Peg p : colors)
		{
			if(!p.isValidCodePeg())
			{	throw new Exception();	}
		}
	}
	
	/**
	 * Return the color at the specified index as its char value
	 * @param index location of the desired color char
	 * @return the character representing the color
	 */
	public char colorAt(int index)
	{
		return colors.get(index).getColor();
	}
	
	/**
	 * Converts Code back into a sequence of characters to be displayed.
	 */
	@Override
	public String toString()
	{
		return colors.parallelStream()
				.map(peg -> peg.toString())
				.sequential()
				.collect(Collectors.joining(""))
				.toString();
	}
	
	/**
	 * Generate a random code from the valid peg colors.
	 * @return the generated code
	 */
	public static Code getRandomCode()
	{
		StringBuilder codeString = new StringBuilder();
		Random rand = new Random();
		for(int i = 0; i < CODE_LENGTH; i += 1)
		{
			codeString.append(Peg.VALID_CODE_COLORS[ rand.nextInt(Peg.VALID_CODE_COLORS.length) ]);
		}
		return new Code(codeString.toString());
	}
	
	@Override
	public boolean equals(Object obj)
	{
		return obj instanceof Code && toString().equals(((Code)obj).toString());
	}
	
	@Override
	public int hashCode()
	{
		return colors.parallelStream()
				.map(peg -> (int)peg.getColor())
				.reduce(0, (a, b) -> a + b);
	}
}
