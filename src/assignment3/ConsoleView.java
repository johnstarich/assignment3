package assignment3;

import java.util.Scanner;

/**
 * View designed to prompt and print out using the console.
 * @author johnstarich
 */
public class ConsoleView extends View
{
	/** Method of reading in data from the player. */
	private Scanner in = new Scanner(System.in);
	
	/**
	 * Creates a ConsoleView which will prompt and show the user with information from the standard output stream, or "console".
	 * @param game The game being played
	 */
	public ConsoleView(Game game)
	{	super(game);	}

	/**
	 * Print out all of the previous guesses and their results.
	 */
	@Override
	public void showHistory()
	{
		game.getHistory().forEach((code, result) ->
		{
			System.out.println(code+" = "+result);
		});
		System.out.println();
	}

	/**
	 * Print out that the player has entered an invalid guess.
	 */
	@Override
	public void showInvalidInput()
	{
		System.out.println("INVALID GUESS\n");
	}

	/**
	 * Print out that the player has entered a guess in more than once.
	 */
	@Override
	public void showRepeatedInput()
	{
		System.out.println("Repeated guess");
	}

	/**
	 * Print out the result from the guess.
	 */
	@Override
	public void showResult(Code guess, Result result)
	{
		System.out.println(guess + " -> Result: " + result+"\n");
	}

	/**
	 * Prompt the user for a code guess.
	 */
	@Override
	public String askForGuess()
	{
		System.out.print("What is your next guess?\nType in the characters for your guess and press enter:\nEnter guess: ");
		String input = in.nextLine();
		if(input == null)
		{	return "";	}
		else
		{	return input;	}
	}

	/**
	 * Prompt the user if they would like to start a new game.
	 * Y, y, and yes will return true, false otherwise
	 */
	@Override
	public boolean askForNewGame()
	{
		System.out.print("Would you like to play a new game? [Y/N] ");
		String input = in.nextLine();
		if(input == null)
		{	return false;	}
		return input.matches("Y|y|yes");
	}

	/**
	 * Print out that the player won!
	 */
	@Override
	public void showWin()
	{
		System.out.println("You won!!!!");
	}

	/**
	 * Print out that the player lost.
	 */
	@Override
	public void showLose()
	{
		System.out.println("You lost.");
	}

	/**
	 * Print out the player's remaining number of guesses
	 */
	@Override
	public void showRemainingGuesses()
	{
		System.out.println("You have " + game.getRemainingGuesses() + " guesses left.");
	}
}
