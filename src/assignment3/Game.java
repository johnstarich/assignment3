package assignment3;

import java.util.LinkedHashMap;

/**
 * The Mastermind Game
 * John Starich and Joshua Gnanayutham 
 * Section 16005
 * @author johnstarich
 */
public class Game
{
	/** Randomly generated code the player must attempt to guess. */
	private Code secretCode = Code.getRandomCode();
	public static int INITIAL_REMAINING_GUESSES = 15;
	/** Remaining number of guesses the player has before they lose. */
	private int remainingGuesses = INITIAL_REMAINING_GUESSES;
	/** Guess/result pairs for each attempt at finding out the secret code. */
	private LinkedHashMap<Code, Result> history = new LinkedHashMap<>();
	/** Determines whether or not to show the secret code while playing the game. */
	private boolean testMode;
	/** Interface for the player to interact with as the game runs. */
	private View view;
	
	/**
	 * Construct a Game and set whether or not it will run in test mode and print out the secret code.
	 * @param testMode determines whether or not to show the secret code while playing the game
	 */
	public Game(boolean testMode)
	{	this.testMode = testMode;	}
	
	/**
	 * Remaining guesses player has to figure out the code.
	 * @return number of remaining guesses
	 */
	public int getRemainingGuesses()
	{	return remainingGuesses;	}

	/**
	 * Asks the player if they would like to play a game.
	 * @return true if they want to play a game, false otherwise
	 */
	public boolean shouldPlayNewGame()
	{
		return view.askForNewGame();
	}
	
	/**
	 * Returns the secret code for this game.
	 * @return the secret code
	 */
	public Code getSecret()
	{	return secretCode;	}
	
	/**
	 * Returns the name of this game.
	 * @return name of this game
	 */
	public String getName()
	{	return "Mastermind";	}
	
	/**
	 * Guess/result pairs for each attempt at finding out the secret code.
	 * @return guess/result pairs
	 */
	public LinkedHashMap<Code, Result> getHistory()
	{	return history;	}
	
	/**
	 * Return the rules for this game.
	 * @return the rules
	 */
	public String getRules()
	{
		return	"Welcome to Mastermind. Here are the rules.\n"
				+ "This is a text version of the classic board game Mastermind.\n"
				+ "The computer will think of a secret code. The code consists of 4 colored pegs.\n"
				+ "The pegs MUST be one of six colors: blue, green, orange, purple, red, or yellow."
				+ " A color may appear more than once in the code. You try to guess what colored pegs are in"
				+ " the code and what order they are in. After you make a valid guess the result (feedback) will be displayed.\n"
				+ "The result consists of a black peg for each peg you have guessed exactly correct"
				+ " (color and position) in your guess. For each peg in the guess that is the correct color, but"
				+ " is out of position, you get a white peg. For each peg, which is fully incorrect, you get no feedback.\n"
				+ "Only the first letter of the color is displayed. B for Blue, R for Red, and so forth.\n"
				+ "When entering guesses you only need to enter the first character of each color as a capital letter.";
	}
	
	/**
	 * Decrements the number of guesses by one and returns if there are any turns remaining.
	 * @return true if there are guesses left, false otherwise
	 */
	public boolean decrementGuesses()
	{	return --remainingGuesses > 0;	}
	
	private boolean running = true;
	
	/**
	 * Identifies if the game is still running.
	 * @return true if running, false otherwise
	 */
	public boolean isRunning()
	{	return running;	}
	
	/**
	 * Tells the game to stop running and dispose of its view.
	 */
	public void stopRunning()
	{
		view.close();
		running = false;
	}
	
	/**
	 * Begin the game.
	 */
	public void runGame()
	{
		view = new SwingView(this);
		if(testMode)
		{	System.out.println("Secret Code: "+secretCode);	}
	}
}
