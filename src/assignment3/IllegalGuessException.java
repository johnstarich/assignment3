package assignment3;

/**
 * Thrown if a player's guess is incorrectly formatted.
 * @author johnstarich
 */
public class IllegalGuessException extends Exception 
{
	/** Override needed for any extension to an Exception. */
	private static final long serialVersionUID = -3447529549353991604L;
	
	@Override
	public String getMessage()
	{
		return "Invalid Guess. Try again!";
	}
	
}
