package assignment3;

import javax.swing.JOptionPane;

/**
 * Test class for running Game
 * @author johnstarich
 */
public class Main
{
	public static void main(String[] args) throws InterruptedException
	{
		boolean testMode = true;
		
		/* testMode does not matter for this temporary instance of game */
		Game game;
		do {
			game = new Game(testMode);
			game.runGame();
			while(game.isRunning())
			{	Thread.sleep(100);	}
		}
		while(game.shouldPlayNewGame());
		
		JOptionPane.showMessageDialog(null, "Thanks for playing "+game.getName()+".");
	}
}
