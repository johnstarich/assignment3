package assignment3;

import java.awt.Graphics2D;

/**
 * Generic class to enable objects to be painted.
 * @author johnstarich
 */
public interface Paintable
{
	/**
	 * Paint this using the provided graphics
	 * @param graphics the provided graphics used to paint
	 */
	public void paint(Graphics2D graphics);
}
