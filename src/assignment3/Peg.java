package assignment3;

import java.awt.Color;

/**
 * Contains and validates colored pegs used in Mastermind.
 * @author johnstarich
 */
public class Peg
{
	/** Valid colors for codes. */
	public static final char[] VALID_CODE_COLORS = { 'B', 'G', 'O', 'P', 'R', 'Y', 'M' };
	/** Valid colors for results. */
	public static final char[] VALID_RESULT_COLORS = { 'B', 'W', '-' };
	
	/** The character representing this color (could be an invalid character). */
	private char color;
	
	/**
	 * Creates a peg with the provided character as its color. The color may be invalid.
	 * @param color the provided color
	 */
	public Peg(char color)
	{
		this.color = color;
	}
	
	/**
	 * Return this peg's color
	 * @return this peg's color
	 */
	public char getColor()
	{
		return color;
	}
	
	/**
	 * Validates this peg if it is one of the valid code colors.
	 * @return true if valid, false otherwise
	 */
	public boolean isValidCodePeg()
	{
		for(char c : VALID_CODE_COLORS)
		{
			if(color == c)
			{	return true;	}
		}
		return false;
	}
	
	/**
	 * Validates this peg if it is one of the valid result colors.
	 * @return true if valid, false otherwise
	 */
	public boolean isValidResultPeg()
	{
		for(char c : VALID_RESULT_COLORS)
		{
			if(color == c)
			{	return true;	}
		}
		return false;
	}
	
	/**
	 * Converts this peg into a color string (of length 1).
	 */
	@Override
	public String toString()
	{
		return color+"";
	}
	
	/** The specialized color for maroon. */
	private static final Color MAROON = new Color(0x7B1113);
	
	/**
	 * Return the appropriate color for the provided character
	 * @param c the provided character
	 * @return the color corresponding the character
	 */
	public static Color colorForChar(char c)
	{
		switch(c)
		{
		case 'B': return Color.BLUE;
		case 'G': return Color.GREEN;
		case 'O': return Color.ORANGE;
		case 'P': return Color.MAGENTA;
		case 'R': return Color.RED;
		case 'Y': return Color.YELLOW;
		case 'M': return MAROON;
		default : return Color.BLACK;
		}
	}
}
