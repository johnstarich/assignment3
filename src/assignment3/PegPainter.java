package assignment3;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

/**
 * Paints a peg for Mastermind.
 * @author johnstarich
 */
public class PegPainter implements Paintable
{
	/** The desired width for the peg to be drawn. */
	public static final int PEG_WIDTH = 30;
	/** The desired height for the peg to be drawn. */
	public static final int PEG_HEIGHT = 40;
	private Color color;
	private double scale = 5;
	
	/**
	 * Creates a PegPainter with the provided color
	 * @param color the provided color
	 */
	public PegPainter(Color color)
	{
		this.color = color;
	}
	
	/**
	 * Creates a PegPainter with the provided color and scale
	 * @param color the provided color
	 * @param scale the provided scale
	 */
	public PegPainter(Color color, double scale)
	{
		this(color);
		this.scale = scale;
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.scale(scale, scale);
		//shadow
		g.setColor(Color.GRAY);
		g.fillOval(getUnitWidth()*3/16, getUnitHeight()*3/8, getUnitWidth(), 2);
		//peg fill
		g.setColor(color);
		g.fillArc(0, 0, getUnitWidth(), getUnitHeight(), 0, 180);
		g.fillArc(0, getUnitHeight()*3/8, getUnitWidth(), getUnitHeight()/4, 0, -180);
		//peg stroke
		g.setColor(g.getColor().darker());
		g.setStroke(new BasicStroke(0.25f));
		g.drawArc(0, 0, getUnitWidth(), getUnitHeight(), 0, 180);
		g.drawArc(0, getUnitHeight()*3/8, getUnitWidth(), getUnitHeight()/4, 0, -180);
	}
	
	/**
	 * Returns the unit width for this page so that it may be scaled.
	 * @return the unit width
	 */
	public int getUnitWidth()
	{
		return (int)(PEG_WIDTH/scale);
	}
	
	/**
	 * Returns the unit height for this page so that it may be scaled.
	 * @return the unit height
	 */
	public int getUnitHeight()
	{
		return (int)(PEG_HEIGHT/scale);
	}
}
