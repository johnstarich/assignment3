package assignment3;

/**
 * Number of black pegs and white pegs resulting 
 * from a guess compared to a secret code
 * @author Kevin Sy
 *
 */
public class Result 
{
	/** number of black pegs*/
	private int blackPegs;
	/** number of white pegs*/
	private int whitePegs;
	
	/**
	 * Creates the Result with provided number of pegs
	 * @param blackPegs number of black pegs
	 * @param whitePegs number of white pegs
	 */
	public Result(int blackPegs, int whitePegs)
	{
		this.blackPegs = blackPegs;
		this.whitePegs = whitePegs;
	}
	
	/**
	 * This Result's number of black pegs
	 * @return the number of black pegs
	 */
	public int getBlackPegs()
	{
		return blackPegs;
	}
	
	/**
	 * This Result's number of white pegs
	 * @return the number of white pegs
	 */
	public int getWhitePegs()
	{
		return whitePegs;
	}

	/**
	 * Calculate the number of black pegs
	 * @param guesscopy - copy of the guess for eliminating characters
	 * @param secretcopy - copy of the secret for eliminating characters
	 * @return numBlackPegs - how many pegs were guess correctly (exact position and color)
	 */
	private static int calcNumberOfBlackPegs(char[] guesscopy,char[] secretcopy)
	{
		int i = 0 ;
		int numBlackPegs = 0;
		//iterate through the guesscopy and secretcopy
		for (i = 0; i < guesscopy.length; i++)
		{
			if(guesscopy[i] == secretcopy[i])
			{	
				numBlackPegs++;
				guesscopy[i] = '-';// cross off the character
				secretcopy[i] = '-';// cross off the character
			}
		}
		
		return numBlackPegs;
	}
	/**
	 * Calculate the number of white pegs (correct colored pegs but different positions)
	 * @param guesscopy - copy of the guess for eliminating characters
	 * @param secretcopy - copy of the secret for eliminating characters
	 * @return number of white pegs (correct colored pegs but different positions)
	 */
	private static int calcNumberOfWhitePegs(char[] guesscopy,char[] secretcopy)
	{
		int i = 0;
		int j = 0;
		int numWhitePegs = 0;
		
		//iterate through the guess characters
		for(i = 0; i < guesscopy.length; i++)
		{
			if(guesscopy[i] != '-')
			{
				// iterate through the secret
				for(j=0; j < secretcopy.length; j++)
				{
					//Is it a crossed off character?
					if(secretcopy[j] != '-')
					{
						//Are they equal in character values but different in position?
						if(guesscopy[i] == secretcopy[j])
						{
							numWhitePegs++;
							guesscopy[i] = '-'; // cross off character in guesscopy
							secretcopy[j] = '-';// cross off character in secretcopy
						}
					}
				}
			}
		}
		return numWhitePegs;
	}
	
	/**
	 * Calculate the number of black pegs and white pegs
	 * @param guess - user input guess
	 * @param secret - secret code that needs
	 * @return Result containing the black pegs and white pegs
	 */
	public static Result compareCodeWithSecret(Code guess, Code secret)
	{
		//resetting to zero
		int blackPegs = 0;
		int whitePegs = 0;
		
		//Copy of Code guess and Code secret as a charArray
		char[] guessCopy = guess.toString().toCharArray();
		char[] secretCopy = secret.toString().toCharArray();
		
		//Calculate the number of black pegs
		blackPegs = calcNumberOfBlackPegs(guessCopy,secretCopy);
		//Calculate the number of white pegs
		whitePegs = calcNumberOfWhitePegs(guessCopy,secretCopy);
		
		return new Result(blackPegs, whitePegs);
	}
	
	/**
	 * Determines whether they win or not
	 * @return true if they win, false otherwise
	 */
	public boolean isWin()
	{	return blackPegs == Code.CODE_LENGTH;	}

	
	/**
	 * Determines how to display the number of black pegs and white pegs
	 */
	@Override
	public String toString()
	{
		if(blackPegs > 0 && whitePegs == 0 )
		{	//nonzero number of black pegs and zero white pegs
			return blackPegs +" black pegs";
		}
		else if (blackPegs == 0 && whitePegs > 0)
		{	// zero number of black pegs and nonzero number of white pegs
			return whitePegs +" white pegs";
		}
		else if (blackPegs > 0 && whitePegs > 0)
		{	// nonzero number of black and white pegs
			return blackPegs + " black pegs, " + whitePegs +" white pegs";
		}
		else if(blackPegs == 0 && whitePegs == 0)
		{	// zero number of black and white pegs
			return "No pegs";
		}
		else
		{
			return "Error counting black and white pegs";
		}
	}
}
