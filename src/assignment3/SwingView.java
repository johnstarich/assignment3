package assignment3;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.*;

import javax.swing.*;

/**
 * Sets up and enables interfacing with the player in a GUI for Mastermind.
 * @author johnstarich
 */
public class SwingView extends View
{
	private JFrame frame;
	private JPanel input;
	private Board painter;
	private ActionListener endGameListener;
	private JScrollPane scrollPane;
	
	/**
	 * Creates a SwingView with a back-reference to the given game.
	 * @param game the provided game
	 */
	public SwingView(Game game)
	{
		super(game);
		frame = new JFrame("Mastermind");
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		painter = new Board(game);
		
		scrollPane = new JScrollPane(painter, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		//scrollPane.setSize(painter.getPreferredSize().width, 200);
		scrollPane.setPreferredSize(new Dimension(painter.getPreferredSize().width, 400));
		scrollPane.getViewport().setViewPosition(new Point(0, 400));
		
		frame.add("Center", scrollPane);
		
		input = new JPanel();
		frame.add("South", input);
		JOptionPane option = new JOptionPane("<html><h2>Welcome to Mastermind.</h2></html>", JOptionPane.INFORMATION_MESSAGE);
		ImageIcon icon = new ImageIcon("img/mastermind.jpg");
		option.setIcon(new ImageIcon(icon.getImage().getScaledInstance(100, 80, Image.SCALE_SMOOTH)));
		input.add(option);
		
		JButton endGame = new JButton("End Game");
		endGameListener = getEndGameListener();
		
	
		endGame.addActionListener(endGameListener);
		JButton viewRules = new JButton("View Rules");
		viewRules.addActionListener(getRulesListener());
		JButton play = new JButton("Play!");
		play.addActionListener(getPlayListener());
		option.setOptions(new Object[]
		{
			endGame,
			viewRules,
			play
		});
		
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
	/**
	 * Sets up the input pane for player guesses.
	 */
	public void setGuessInputPane()
	{
		JPanel pane = new JPanel();
		pane.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = 1;
		constraints.gridy = 0;
		pane.add(new JLabel("Enter your guess:"), constraints);
		JTextField field = new JTextField(10);
		constraints.gridy = 1;
		pane.add(field, constraints);
		JButton guess = new JButton("Guess");
		guess.addActionListener(getGuessListener(field));
		constraints.gridx = 0;
		constraints.gridy = 2;
		pane.add(guess, constraints);
		
		JButton endGame = new JButton("End Game");
		endGame.addActionListener(endGameListener);
		constraints.gridx = 2;
		pane.add(endGame, constraints);
		
		field.addKeyListener(new KeyListener()
		{
			public void keyTyped(KeyEvent e) {}
			public void keyReleased(KeyEvent e) {}
			
			public void keyPressed(KeyEvent e)
			{
				if(e.getKeyCode() == KeyEvent.VK_ENTER)
					guess.doClick();
				else if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
					endGame.doClick();
			}
		});
		
		input.removeAll();
		input.add("South", pane);
		field.requestFocus();
		input.validate();
		input.repaint();
	}
	
	@Override
	public void showHistory()
	{
		StringBuilder historyString = new StringBuilder("Your history: \n");
		game.getHistory().forEach((code, result) ->
		{
			historyString.append(code+" = "+result+"\n");
		});
		JOptionPane.showMessageDialog(painter, historyString);
		setGuessInputPane();
	}

	@Override
	public void showInvalidInput()
	{
		painter.repaint();
		JOptionPane.showMessageDialog(painter, "Invalid input. Please try again.");
		setGuessInputPane();
	}

	@Override
	public void showRepeatedInput()
	{
		JOptionPane.showMessageDialog(painter, "Repeated input. You've entered that guess before, try again.");
		setGuessInputPane();
	}

	@Override
	public void showWin()
	{
		JOptionPane.showMessageDialog(painter, "Congratulations! You won!");
		game.stopRunning();
	}

	@Override
	public void showLose()
	{
		JOptionPane.showMessageDialog(painter, "You lose.");
		game.stopRunning();
	}

	@Override
	public void showRemainingGuesses()
	{
		JOptionPane.showMessageDialog(painter, "You have "+game.getRemainingGuesses()+" guesses remaining.");
	}

	@Override
	public void showResult(Code guess, Result result)
	{
		painter.addGuess(guess, result);
		setGuessInputPane();
		scrollPane.getViewport().setViewPosition(
			new Point(
					0,
					(game.getRemainingGuesses()-1)*(PegPainter.PEG_HEIGHT+5)
				)
			);
	}

	@Override
	public String askForGuess()
	{
		return JOptionPane.showInputDialog(painter, "What's your guess?");
	}

	@Override
	public boolean askForNewGame()
	{
		boolean response = JOptionPane.showConfirmDialog(painter, "Would you like to play a new game?") == 0;
		game.stopRunning();
		return response;
	}

	/**
	 * Creates and returns a new ActionListener for when clicking the End Game button
	 * Stops the game and disposes of the window.
	 * @return the new listener
	 */
	public ActionListener getEndGameListener()
	{
		return new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				game.stopRunning();
			}
		};
	}
	
	/**
	 * Creates and returns a new ActionListener for when clicking the View Rules button
	 * @return the new listener
	 */
	public ActionListener getRulesListener()
	{
		return new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				JOptionPane.showMessageDialog(painter,
					"<html><body><h2 style='margin: 0;'>Rules:</h2><p style='width:400px;'><br>"
						+ game.getRules().replace("\n", "<br>") + "</p></body></html>"
					);
			}
		};
	}
	
	/**
	 * Creates and returns a new ActionListener for when clicking the Play button
	 * Sets up the guess input pane for entering guesses.
	 * @return the new listener
	 */
	public ActionListener getPlayListener()
	{
		return new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				setGuessInputPane();
			}
		};
	}
	
	/**
	 * Creates and returns a new ActionListener for when clicking the Guess button
	 * Evaluates the guess for validity and even handles when entering history. (Uses legacy history output)
	 * @param field the JTextField containing the guess text
	 * @return the new listener
	 */
	public ActionListener getGuessListener(JTextField field)
	{
		return new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				Code guess = new Code( field.getText() );
				if(guess.toString().equals("history"))
				{
					showHistory();
					return;
				}
				try { guess.validate();}
				catch(Exception e1)
				{
					showInvalidInput();
					return;
				}
				if(game.getHistory().containsKey(guess))
				{
					showRepeatedInput();
					return;
				}
				
				boolean guessesRemaining = game.decrementGuesses();
				Result result = Result.compareCodeWithSecret(guess, game.getSecret());
				showResult(guess, result);
				
				game.getHistory().put(guess, result);
				
				if(result.isWin())
				{
					showWin();
				}
				else if(! guessesRemaining)
				{
					showLose();
				}
			}
		};
	}
	
	@Override
	public void close()
	{
		frame.dispose();
	}
}

