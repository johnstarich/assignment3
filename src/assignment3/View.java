package assignment3;

/**
 * Structure used to interface with the player while playing a game.
 * @author johnstarich
 */
public abstract class View
{
	/**
	 * The game this view is attached to. Used for grabbing the history from, for example.
	 */
	protected Game game;
	
	/**
	 * Creates a view and points back at its attached game.
	 * @param game the attached game
	 */
	public View(Game game)
	{
		this.game = game;
	}
	
	/**
	 * Shows the history of the player's guesses and their results.
	 */
	public abstract void showHistory();
	
	/**
	 * Shows the player that their input was not valid.
	 */
	public abstract void showInvalidInput();
	
	/**
	 * Shows the player that their input has been entered before.
	 */
	public abstract void showRepeatedInput();
	
	/**
	 * Show the player that they won!
	 */
	public abstract void showWin();
	
	/**
	 * Show the player that they lost.
	 */
	public abstract void showLose();
	
	/**
	 * Show the player the number of remaining guesses.
	 */
	public abstract void showRemainingGuesses();
	
	/**
	 * Shows the player the result of their guess.
	 * @param guess the entered guess
	 * @param result the result to show the player
	 */
	public abstract void showResult(Code guess, Result result);
	
	/**
	 * Prompts the player for a guess at the code.
	 * @return the player's input
	 */
	public abstract String askForGuess();
	
	/**
	 * Prompts the player if they would like to start a new game.
	 * @return true if they would like to start a new game, false otherwise
	 */
	public abstract boolean askForNewGame();
	
	/**
	 * Closes the view and disposes of any open resources.
	 */
	public void close() {}
}
